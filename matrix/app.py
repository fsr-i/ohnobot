
#!/usr/bin/env python3

import pprint
pp = pprint.PrettyPrinter(indent=2).pprint

import asyncio
from PIL import Image
from nio import (AsyncClient, ClientConfig, RoomMessageText, InviteEvent)
import requests

import re, yaml, magic, random, glob
import os, io, sys, signal
dir_path = os.path.dirname(os.path.realpath(__file__))


config = yaml.load(open("config.yaml"), Loader=yaml.SafeLoader)
bot = AsyncClient(
	config['homeserver_url'],
	config['username']#,
	#device_id=config['device_id'],
	#store_path=dir_path,
	#config=ClientConfig(store_sync_tokens=True)
)

ohno_re = re.compile('^(.+[^a-z]|[^a-z]?)o+c*h+\s*n+(o+|ö+|e+i+n+)([^a-z]?|[^a-z].+)$', flags=re.IGNORECASE)
#comic_re = re.compile('><img src=\"(https:\/\/[0-9]+\.media\.tumblr\.com\/[^\"]+\..{2,5})\" data-orig-', flags=re.IGNORECASE)
comic_re = re.compile('data-image=\"(https:\/\/assets\.amuniversal\.com\/[0-9a-z]+)\"', flags=re.IGNORECASE)
comic_url = 'https://www.gocomics.com/random/webcomic-name'


async def message_cb(room, event):
	if not event.sender.startswith('@'+config['username']+':'):
		args = event.body.split()
		isitme = list(filter(lambda x: x.startswith('@'+config['username']+':') or x == '!ohno', args))
		args = list(filter(lambda x: x not in isitme, args))
		if len(isitme) > 0:
			print("oh, it's my turn: "+str(args))
			if len(args) > 0 and args[0] == 'comic':
				print("  let's find a comic!")
				r = requests.get(comic_url)
				if r.ok:
					image_list = comic_re.findall(r.text)
					if len(image_list) > 0:
						# Well, by now there is only one image, so...
						#comic_strip = image_list[random.randint(0,len(image_list)-1)]
						comic_strip = image_list[0]
						print('   getting '+comic_strip+' ...')
						image = requests.get(comic_strip)
						if image.ok:
							with Image.open(io.BytesIO(image.context)) as img:
								mime = img.get_format_mimetype()
								dim = img.size
							response = await bot.upload(image.content, mime)
							print(await bot.room_send(room.room_id, 'm.room.message', {
								"body": comic_strip.split('/')[-1],
								"msgtype": "m.image",
								"url": response.content_uri,
								'info': {
									'size': len(image.content),
									'mimetype': mime,
									'w': dim[0],
									'h': dim[1]
								}
							}))
							return
							
				print('  no images found.')
				print(await bot.room_send(room.room_id, 'm.room.message', {
					"body": "Oh no, I couldn't find the comic...",
					"msgtype": "m.text"
				}))
				return
				
		elif ohno_re.match(event.body):
			#print(
			#	"Message received for room {} | {}: {}".format(
			#		room.display_name, room.user_name(event.sender), pp(event)
			#	)
			#)
			print('  lets send an image!')
			image_list = [item for i in [glob.glob('img/ohno/*.%s' % ext) for ext in ["jpg","gif","png","tga"]] for item in i]
			if len(image_list) > 0:
				local_file = image_list[random.randint(0,len(image_list)-1)]
				print('   sending '+local_file)

				with Image.open(local_file) as img:
					mime = img.get_format_mimetype()
					dim = img.size

				with open(local_file, 'rb') as f:
					fsize = os.fstat(f.fileno()).st_size
					response = await bot.upload(f, mime)

				print(await bot.room_send(room.room_id, 'm.room.message', {
						"body": "ohno.png",
						"msgtype": "m.image",
						"url": response.content_uri,
						"info": {
							'size': fsize,
							'mimetype': mime,
							'w': dim[0],
							'h': dim[1]
						}
					}))
					return
			else:
				print('  no images found. sending text "ohno"')
				print(await bot.room_send(room.room_id, 'm.room.message', {
					"body": "ohno",
					"msgtype": "m.text"
				}))
			return
		print('    [ boring message by '+event.sender+' ]')


async def invite_cb(room, event):
		print(
			"{} invited me to room {}! (I think) {}".format(
				room.user_name(event.sender), room.display_name, pp(room)
			)
		)
		await bot.join(room.room_id)


bot.add_event_callback(message_cb, RoomMessageText)
bot.add_event_callback(invite_cb, InviteEvent)

async def main():
		await bot.login(config['password'])
		await bot.sync_forever(timeout=30000, full_state=True)
		print('bot.sync_forever stopped on its own!')


try:
	#signal.signal(signal.SIGINT, lambda a,b: await bot.logout())
	asyncio.get_event_loop().run_until_complete(main())
except KeyboardInterrupt:
	# console just printed '^C', so omit the 'C'
	print('leaning up...')
except Exception as e:
	dir(e)
finally:
	asyncio.get_event_loop().run_until_complete(bot.logout())
	asyncio.get_event_loop().run_until_complete(bot.close())
	sys.exit(0)
